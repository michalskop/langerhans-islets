import base64
from flask import Flask, render_template
from flask_socketio import SocketIO, emit, send
import json
import random
import time

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, cors_allowed_origins="*")


@socketio.on('client_connected')
def handle_my_custom_event(json):
    emit({'data': 'Hi from Server'})
    print('received json: ' + str(json))
    position = {'x': 0, 'y': 0}
    maxstep = 0.5
    limit = 1.3
    while True:
        position = {
            'x': random.triangular(max(-1 * limit, position['x'] - maxstep), min(limit, position['x'] + maxstep), position['x']),
            'y': random.triangular(max(-1 * limit, position['y'] - maxstep), min(limit, position['y'] + maxstep), position['y'])
        }
        emit('position', position)

        fname = 'images/207iz190205-SZ60-zv15x_' + str(random.randint(21, 25)) + '.jpg'
        with open(fname, 'rb') as f:
            image_data = f.read()
        encoded_string = base64.b64encode(image_data)
        emit('image data', {'image_data': encoded_string})
        print(fname)
        time.sleep(5)


# https://tutorialedge.net/python/python-socket-io-tutorial/
@socketio.on('message')
def print_message(message):
    # # it will forward the json to all clients.
    # send(message)
    print('received message: ' + str(message))
    emit({'data': 'Hi from Server'})
    emit('message', message[::-1])


@socketio.on('my event')
def handle_my_event(s):
    data = json.loads(s)
    print('received json: ' + str(data))
    print('received key message ' + data['message'])
    emit('my reply', {'message': 'xxx'})


@socketio.on('alert')
def handle_my_alert(s):
    print('received alert from client:' + s)
    emit('alert', 'Fire! Fire! Fire!')

# @socketio.on('alert_button')
# def handle_alert_button(json):
#     # it will forward the json to all clients.
#     print('Message from client was {0}'.format(json))
#     emit('alert', 'Message from backend')




if __name__ == '__main__':
    socketio.run(app, host='192.168.42.147', port=5000)
    # socketio.run(app)
