from flask import Flask, render_template,jsonify,request
from PIL import Image
import PIL
import io
import base64

app = Flask(__name__)

@app.route("/image_info",methods= ['GET'])
def image_info():
    myfile= request.args.get('myimage').split(',')
    imgdata = base64.b64decode(myfile[1])
    with open("/home/michal/dev/langerhans-islets/PicClickAuto/images/image.jpg", "wb") as fout:
        fout.write(imgdata)
    im = Image.open(io.BytesIO(imgdata))
    width, height = im.size
    imgformat=im.format
    return _corsify_actual_response(jsonify(width=width,height=height,imgformat=imgformat))

@app.route("/")
def photoClick():
    return render_template('photoclick.html')


def _build_cors_prelight_response():
    response = make_response()
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add('Access-Control-Allow-Headers', "*")
    response.headers.add('Access-Control-Allow-Methods', "*")
    return response

def _corsify_actual_response(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


if __name__ == '__main__':
   app.run(debug = True)
